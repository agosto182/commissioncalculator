import express from 'express';
import { connect } from './src/db.js';
import router from './src/routes.js';

// Connection to the database
connect();
const app = express();
const port = 3000;

app.use(express.json());
app.use(router);

app.listen(port, () => {
  console.log(`App is running on port ${port}`)
});

export default app;
