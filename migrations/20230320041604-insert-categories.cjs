'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    const date = new Date();
    queryInterface.bulkInsert("Categories", [
      {
        key: "C83",
        description: "Material de oficina",
        iva: 0.16,
        createdAt: date,
        updatedAt: date
      },
      {
        key: "Y3",
        description: "Consumibles",
        iva: 0.16,
        createdAt: date,
        updatedAt: date
      },
      {
        key: "RF95",
        description: "Material radioactivo",
        iva: 0.35,
        createdAt: date,
        updatedAt: date
      },
      {
        key: "PX112",
        description: "Productos de supervivencia zombie",
        iva: 0,
        createdAt: date,
        updatedAt: date
      },
      {
        key: "JF42",
        description: "Ositos de peluche para mascotas",
        iva: 0.12,
        createdAt: date,
        updatedAt: date
      },
    ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    queryInterface.dropTable('Categories')
  }
};
