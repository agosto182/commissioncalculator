import { getInvoices } from './invoice.service.js';

const COMISSION_LIMIT = 300000;

const SPECIAL_CLIENTS = {
  ABCD001122AA1: (_) => 0.06,
  WXYZ221105AB1: (c) => (c > 0.06 ? c : 0.06),
  CLCL170115HBL: (c) => (c > 0.07 ? 0.07 : c),
  AXCV891225X1D: (_) => 0,
};

export function checkIfSpecialClient(rfc, commision) {
  return SPECIAL_CLIENTS[rfc] ? SPECIAL_CLIENTS[rfc](commision) : commision;
}

function calculateSpecialFee(invoice, subtotal, commissions) {
  let commission = commissions[0];
  let fee = 0;

  if (subtotal / invoice.subTotal <= 4) {
    commission = commissions[1];
  } else if (hasOfficeAndSupplyCategory(invoice)) {
    commission = commissions[2];
  } else {
    const taxPercent = (100 / invoice.subTotal) * invoice.taxes;
    if (taxPercent > 5 && taxPercent < 11) {
      commission = commissions[3];
    }
  }

  commission = checkIfSpecialClient(invoice.rfc, commission);

  console.info('Commission is ', commission);

  for (const detail of invoice.details) {
    fee += detail.subTotal * commission;
  }

  return fee;
}

export function hasOfficeAndSupplyCategory(invoice) {
  const hasOfficeCategory = invoice.details.find((d) => d.category == 'C83');
  const hasSuppliesCategory = invoice.details.find((d) => d.category == 'Y3');

  return hasOfficeCategory && hasSuppliesCategory;
}

async function calculateComission(firstYear = false) {
  const invoices = await getInvoices();

  const subtotal = invoices.reduce((acum, i) => acum + i.subTotal, 0);

  const result = {
    type: 'SELLER_D',
    totalSold: 0,
    totalFees: 0,
    details: [],
  };

  if (subtotal < COMISSION_LIMIT) {
    for (const invoice of invoices) {
      let commission = 0.045; // 4.5 of commission
      let fee = 0;

      if (invoice.subTotal < 20000) {
        commission = 0.025; // 2.5 of commission
      } else if (hasOfficeAndSupplyCategory(invoice)) {
        commission = 0.04; // 4 of commission
      }

      commission = checkIfSpecialClient(invoice.rfc, commission);

      console.info('Commission is ', commission);

      for (const detail of invoice.details) {
        if (detail.category !== 'RF95') {
          fee += detail.subTotal * commission;
        }
      }

      result.details.push({
        invoiceId: invoice.id,
        fees: fee,
      });
      result.totalFees += fee;
      result.totalSold += invoice.subTotal;
    }
  } else if (firstYear) {
    for (const invoice of invoices) {
      let commission = 0.055; // 5.5 of commission
      let fee = 0;
      const categories = invoice.details.reduce((acum, d) => {
        if (acum.indexOf(d.category) == -1) {
          acum.push(d.category);
        }

        return acum;
      }, []);

      if (categories.length >= 4) {
        commission = 0.063;
      } else if (invoice.subTotal >= 200000) {
        commission = 0.1;
      }

      commission = checkIfSpecialClient(invoice.rfc, commission);

      console.info('Commission is ', commission);

      for (const detail of invoice.details) {
        if (detail.category == 'RF95') {
          fee += detail.subTotal * 0.03;
        } else {
          fee += detail.subTotal * commission;
        }
      }

      result.details.push({
        invoiceId: invoice.id,
        fees: fee,
      });
      result.totalFees += fee;
      result.totalSold += invoice.subTotal;
    }
  } else if (subtotal >= COMISSION_LIMIT) {
    let commissions = [0, 0, 0, 0];
    if (invoices.length > 20) {
      // seller type A
      result.type = 'SELLER_TYPE_A';
      commissions = [0.067, 0.08, 0.072, 0.069];
    } else if (invoices.length > 10) {
      // seller type b
      result.type = 'SELLER_TYPE_B';
      commissions = [0.06, 0.07, 0.068, 0.065];
    } else {
      // seller type c
      result.type = 'SELLER_TYPE_C';
      commissions = [0.052, 0.062, 0.059, 0.057];
    }

    for (const invoice of invoices) {
      const fee = calculateSpecialFee(invoice, subtotal, commissions);

      result.details.push({
        invoiceId: invoice.id,
        fees: fee,
      });
      result.totalFees += fee;
      result.totalSold += invoice.subTotal;
    }
  }

  return result;
}

export {
  calculateComission,
};
