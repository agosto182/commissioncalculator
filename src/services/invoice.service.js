import { getConnection } from '../db.js';

async function createInvoice(payload) {
  const { CategoryModel, InvoiceModel } = getConnection();
  const invoice = { ...payload };
  // Calculate all details of the invoice
  for (const detail of invoice.details) {
    const category = await CategoryModel.findOne({
      where: {
        key: detail.category,
      },
    });
    console.log('Category', category);

    detail.subTotal = detail.quantity * detail.unitPrice;
    detail.taxRate = category ? category.iva : 0;
    detail.taxAmount = detail.subTotal * (detail.taxRate);
    detail.total = detail.subTotal + detail.taxAmount;
  }

  invoice.subTotal = invoice.details.reduce((acum, d) => acum + d.subTotal, 0);
  invoice.taxes = invoice.details.reduce((acum, d) => acum + d.taxAmount, 0);
  invoice.total = invoice.details.reduce((acum, d) => acum + d.total, 0);

  console.log('Invoice to create', invoice);
  return InvoiceModel.create(invoice, {
    include: [{
      association: InvoiceModel.details,
    }],
  });
}

async function getInvoices() {
  const { InvoiceDetailModel, InvoiceModel } = getConnection();
  const invoices = await InvoiceModel.findAll({
    include: {
      model: InvoiceDetailModel, as: 'details',
    },
  });

  return invoices;
}

export {
  createInvoice,
  getInvoices,
};
