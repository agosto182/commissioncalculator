import { Router } from 'express';
import { createInvoice, getInvoices } from '../services/invoice.service.js';
import invoiceValidator from '../validations/invoice.validator.js';
import invoiceDetailsValidator from '../validations/invoice-details.validator.js';

const router = Router();

router.get('/', async (_, res) => {
  const invoices = await getInvoices();

  res.send(invoices);
});

router.post('/', async (req, res) => {
  const invoice = req.body;

  const { error: invoiceError } = invoiceValidator.validate(invoice);
  const { error: invoiceDetailsError } = invoiceDetailsValidator.validate(invoice.details);

  if (invoiceError || invoiceDetailsError) {
    return res.status(400).send([invoiceDetailsError, invoiceError]);
  }

  try {
    const result = await createInvoice(invoice);
    return res.send(result);
  } catch (err) {
    console.error(err);
    return res.status(500).send(err);
  }
});

export default router;
