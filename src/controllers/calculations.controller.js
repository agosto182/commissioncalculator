import { Router } from 'express';
import { calculateComission } from '../services/calculation.service.js';

const router = Router();

router.post('/', async (req, res) => {
  const firstYear = req.query.firstYear || false;
  const result = await calculateComission(firstYear);

  res.send(result);
});

export default router;
