import { DataTypes } from 'sequelize';

const InvoiceDetailModel = (sequelize) => sequelize.define('InvoiceDetail', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  category: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  unitPrice: {
    type: DataTypes.DECIMAL,
    allowNull: false,
  },
  quantity: {
    type: DataTypes.DECIMAL,
    allowNull: false,
  },
  subTotal: {
    type: DataTypes.DECIMAL,
    allowNull: false,
  },
  taxRate: {
    type: DataTypes.DECIMAL,
    allowNull: false,
  },
  taxAmount: {
    type: DataTypes.DECIMAL,
    allowNull: false,
  },
  total: {
    type: DataTypes.DECIMAL,
    allowNull: false,
  },
});

export default InvoiceDetailModel;
