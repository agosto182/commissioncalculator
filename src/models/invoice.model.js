import { DataTypes } from 'sequelize';
import { getConnection } from '../db.js';

const InvoiceModel = (sequelize) => {
  const model = sequelize.define('Invoice', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    rfc: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    subTotal: {
      type: DataTypes.DECIMAL,
      allowNull: false,
    },
    taxes: {
      type: DataTypes.DECIMAL,
      allowNull: false,
    },
    total: {
      type: DataTypes.DECIMAL,
      allowNull: false,
    },
  });

  const db = getConnection();

  model.details = model.hasMany(db.InvoiceDetailModel, { as: 'details' });

  return model;
};

export default InvoiceModel;
