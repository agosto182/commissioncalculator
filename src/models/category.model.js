import { DataTypes } from 'sequelize';

const CategoryModel = (sequelize) => sequelize.define('Category', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  key: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  iva: {
    type: DataTypes.DECIMAL,
    allowNull: false,
  },
});

export default CategoryModel;
