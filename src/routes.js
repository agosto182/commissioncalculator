import { Router } from 'express';
import InvoicesController from './controllers/invoices.controller.js';
import CalculationsController from './controllers/calculations.controller.js';

const router = Router();

router.use((_, __, next) => {
  console.log('Time: ', Date.now());
  next();
});

router.get('/', (_, res) => {
  res.send('Welcome :)');
});

router.use('/invoices', InvoicesController);

router.use('/calculate', CalculationsController);

export default router;
