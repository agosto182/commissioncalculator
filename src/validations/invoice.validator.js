import Joi from 'joi';

const schema = Joi.object({
  rfc: Joi.string().required(),
  details: Joi.array(),
});

export default schema;
