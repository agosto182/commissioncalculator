import Joi from 'joi';

const schema = Joi.array().items(
  Joi.object({
    category: Joi.string().required(),
    description: Joi.string().required(),
    unitPrice: Joi.number().required(),
    quantity: Joi.number().required(),
  }),
).required();

export default schema;
