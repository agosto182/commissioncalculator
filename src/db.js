import { Sequelize } from 'sequelize';
import dbConfig from '../database.json' assert { type: 'json' };
import * as models from './models/index.js';

let sequelize = null;
let db = {};

function connect() {
  console.log("models", models)
  const env = process.env.NODE_ENV || 'development';
  const config = dbConfig[env];
  sequelize = new Sequelize(config);

  Object.keys(models).forEach(modelName => {
    db[modelName] = models[modelName](sequelize);
  });
    
  console.log('Connection has been established successfully.');
  if(env == 'development' || env == 'test') {
    sync();
  }
  return db;
}

function sync() {
  sequelize.sync();
}

function getConnection() {
  return db;
}

function disconnect() {
  if(sequelize) {
    console.log("Closing connection");
    sequelize.close();
    sequelize = null;
  }
}

export {
  connect,
  getConnection,
  sync,
  disconnect
}
