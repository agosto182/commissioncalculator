# Commission Calculator

App made in Nodejs with express and sequelize. For save and read invoices and calculate commissions.

## Installation (with docker)

Run docker compose command:
```
$ docker-compose up
```

The application will run on the port 3000.

## Manual Installation

Install dependencies:
```
$ npm install
```

Start the app:
```
$ npm start
```

Run migrations:
```
$ npm run migrations
```

The application will run on the port 3000.

## Testing

To test the application run:

```
$ npm run test
```

## Endpoints

### GET /invoices

Endpoint to get all the invoices in the database.

### POST /invoices

Endpoint to create a new invoice. Expect a JSON in the body.

Body example:

```json
{
  "rfc": "clientrfc",
  "details": [
	{
	  "category": "RF95",
          "description": "Description",
    	  "unitPrice": 1000,
    	  "quantity": 3
	}
  ]
}
```

### POST /calculate

Make calculation of commissions of all the invoices.

To test a seller in the first year in the company. Send the query param `firstYear` as `true`.

Example:
```
$ curl -X POST http://localhost:3000/calculate?firstYear=true
```
