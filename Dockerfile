FROM node:16-alpine

WORKDIR /app

COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json

RUN npm install

COPY ./src /app/src
COPY ./migrations /app/migrations
COPY ./index.js /app/index.js
COPY ./database.json /app/database.json
COPY ./.sequelizerc /app/.sequelizerc

RUN mkdir /app/db/
RUN node -e 'import("./src/db.js").then((m) => m.connect())'
RUN npm run migrations

CMD ["npm", "start"]
