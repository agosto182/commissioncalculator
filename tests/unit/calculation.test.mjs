import {
  checkIfSpecialClient,
  hasOfficeAndSupplyCategory
} from '../../src/services/calculation.service.js';

const MOCK_INVOICE = {
  rfc: "MOCK RFC",
  details: [
    {
      category: "C83",
    },
    {
      category: "Y3",
    }
  ]
}

describe("Unit tests for calculation service", () => {
  it("Should checkIfSpecialClient with rfc WXYZ221105AB1 return 0.06", () => {
    const result = checkIfSpecialClient("WXYZ221105AB1", 0.0599);

    expect(result).toBe(0.06);
  });

  it("Should checkIfSpecialClient with rfc WXYZ221105AB1  return 0.40", () => {
    const result = checkIfSpecialClient("WXYZ221105AB1", 0.40);

    expect(result).toBe(0.40);
  });

  it("Should checkIfSpecialClient with rfc CLCL170115HBL return 0.001", () => {
    const result = checkIfSpecialClient("CLCL170115HBL", 0.001);

    expect(result).toBe(0.001);
  });

  it("Should checkIfSpecialClient with rfc CLCL170115HBL return 0.07", () => {
    const result = checkIfSpecialClient("CLCL170115HBL", 0.1);

    expect(result).toBe(0.07);
  });

  it("Should checkIfSpecialClient with rfc AOEC970208 return 0.40", () => {
    const result = checkIfSpecialClient("AOEC970208", 0.40);

    expect(result).toBe(0.40);
  });

  it("Should hasOfficeAndSupplyCategory return true", () => {
    const mock = {...MOCK_INVOICE};
    const result = hasOfficeAndSupplyCategory(mock);

    expect(result).toBeTruthy();
  });

  it("Should hasOfficeAndSupplyCategory return false", () => {
    const mock = {...MOCK_INVOICE};
    mock.details[0].category = "AA";
    const result = hasOfficeAndSupplyCategory(mock);

    expect(result).toBeFalsy();
  });
})
