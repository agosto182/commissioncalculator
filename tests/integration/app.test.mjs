import app from "../../index.js";
import request from 'supertest';
import { disconnect } from '../../src/db.js';

const MOCK_INVOICE = {
  rfc: "Test",
  details: [{
    category: "C83",
    description: "Some product",
    unitPrice: 1000,
    quantity: 5
  }]
}

describe("Integration test for app", () => {
  afterAll(() => {
    // Close db connection
    disconnect();
  });

  it("POST /invoices - will save a new invoice record", async () => {
    const res = await request(app).post("/invoices").send(MOCK_INVOICE);

    expect(res.statusCode).toBe(200);
  });

  it("GET /invoices - will return all the invoices record", async () => {
    const res = await request(app).get("/invoices");

    expect(res.statusCode).toBe(200);
    expect(res.body.length).toBeGreaterThan(0);
  });

  it("POST /calculcate - will return the calculated commission", async () => {
    const res = await request(app).post("/calculate");

    expect(res.statusCode).toBe(200);
  });
});
